<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\WebhookController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AdditionalController;
use App\Http\Controllers\ConfigController;

Route::get('/', [IndexController::class, 'index'])->middleware(['verify.shopify'])->name('home');
Route::get('/plans', [AdditionalController::class, 'plans'])->middleware(['verify.shopify'])->name('plans');
Route::get('/configs', [IndexController::class, 'configs'])->middleware(['verify.shopify'])->name('configs');
Route::post('/configs', ConfigController::class)->name('configs');

Route::get('/getData', [IndexController::class, 'getData']);
Route::post('/importData', [IndexController::class, 'import']);
Route::get('/cancelPlan', [AdditionalController::class, 'cancelPlan'])->middleware(['verify.shopify'])->name('cancelPlan');

Route::get('/login', function () {

    if (Auth::user()) {
        return redirect()->route('home');
	  }

	  return view('login');

})->name('login');

Route::post('/webhook-order', [WebhookController::class, 'order']);

Route::middleware(['auth.webhook'])->group(function () {
    Route::post('/webhook-unistall', [WebhookController::class, 'unistall']);
    Route::post('/shop_deletion', [WebhookController::class, 'unistall']);
    Route::post('/customer_data_request', [WebhookController::class, 'customer']);
    Route::post('/customer_deletion', [WebhookController::class, 'customerDeletion']);
});
