<?php

namespace App\Http\Controllers;

use App\Models\EcommerceConfig;
use App\Models\Integration;
use App\Models\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $domain = $request->user()->name;
        $token = $request->user()->password;
        $config = EcommerceConfig::where('key', '=', 'token')->where('type_ecommerce', '=', 2)->where('value', '=', $token)->first();

        $url = 'https://'.$domain.'/admin';

        $shop = $request->user();
        if ((!$shop->plan && !$shop->isFreemium() && !$shop->isGrandfathered())) {

            $plans = Plan::with('features')
                ->where('isActive', 1)
                ->where('isAdditional', 0)
                ->where('interval_unit', 'month')
                ->get();

            return view('plans', [
                'user' => $request->user(), 
                'token' => $token,
                'plans' => $plans
            ]);
        }

        if(is_null($config)){
            return view('welcome', ['url' => $url, 'token' => $token]);
        }else{
            $urlInOnexfy = EcommerceConfig::where('key', '=', 'url')->where('type_ecommerce', '=', 2)->where('user_id', '=', $config->user_id)->first();

            if(is_null($urlInOnexfy)){
                EcommerceConfig::create([
                    'key' => 'url', 'value' => $url, 'type_ecommerce' => 2, 'user_id' => $config->user_id
                ]);
            }else{
                $urlInOnexfy->update(['value' => $url]);
            }

            return view('table', ['token' => $token, 'user_id' => $config->user_id]);
        }
    }

    public function configs(Request $request)
    {
        $domain = $request->user()->name;
        $token = $request->user()->password;
        $ecommerce = EcommerceConfig::where('key', '=', 'token')->where('type_ecommerce', '=', 2)->where('value', '=', $token)->first();
        $config = EcommerceConfig::where('key', '=', 'config')->where('user_id', '=', $ecommerce->user_id)->first();

        $url = 'https://'.$domain.'/admin';

        $shop = $request->user();
        if ((!$shop->plan && !$shop->isFreemium() && !$shop->isGrandfathered())) {
            return view('plans', ['user' => $request->user(), 'token' => $token]);
        }

        if(is_null($ecommerce)){
            return view('welcome', ['url' => $url, 'token' => $token]);
        }else{
            $urlInOnexfy = EcommerceConfig::where('key', '=', 'url')->where('type_ecommerce', '=', 2)->where('user_id', '=', $ecommerce->user_id)->first();

            if(is_null($urlInOnexfy)){
                EcommerceConfig::create([
                    'key' => 'url', 'value' => $url, 'type_ecommerce' => 2, 'user_id' => $ecommerce->user_id
                ]);
            }else{
                $urlInOnexfy->update(['value' => $url]);
            }

            return view('config', ['token' => $token, 'user_id' => $ecommerce->user_id, 'config' => $config->value ?? NULL]);
        }
    }    

    public function getData(Request $request)
    {
        $token = $request->token;
        $last_id = isset($request->last_id) ? $request->last_id : 1;
        $type = isset($request->type) ? $request->type : 'products';
        $per_page = isset($request->per_page) ? $request->per_page : 10;
        // $linkBase = 'https://app.onexfy.com/back/public/api/v2/IntegrationServicesToken/'.$type.'?&per_page='.$per_page;
        // $linkBase = 'https://test.onexfy.dev/back/public/api/v2/IntegrationServicesToken/'.$type.'?&per_page='.$per_page;
        $linkBase = 'http://127.0.0.1:8000/api/v2/IntegrationServicesToken/'.$type.'?per_page='.$per_page;

        $link = $linkBase . '&last_id='.$last_id;

        $response = Http::withHeaders([
            'Shopify-Token' => $token,
        ])->get($link);

        $data = $response->json()['data'];

        $model = $type == 'products' ? 'App\Product' : 'App\Category';

        $records = [];
        foreach ($data as $record) {
          $id = $record['id'];
          $integration = Integration::where('external_id', $id)->where('manageable_type', $model)->where('type', 2)->first();
          $record['integrate'] = !is_null($integration);
          $records[] = $record;
          $last_id = $id;
        }
        return json_encode($records);
    }

    public function import(Request $request)
    {
        $token = $request->token;
        $type = $request->type;
        $record = json_decode($request->record);
        $user_id = (int) $request->user_id;
        $fixLink = $type == 'products' ? 'product' : $type;
        // $linkImport = 'https://app.onexfy.com/back/public/api/v2/'.$fixLink.'/importToken';
        // $linkImport = 'https://test.onexfy.dev/back/public/api/v2/'.$fixLink.'/importToken';
        $linkImport = 'http://127.0.0.1:8000/api/v2/'.$fixLink.'/importToken/';

        $http = Http::withHeaders([
            'Shopify-Token' => $token,
        ]);

        if($type == 'products') {
            $data = array(array(
                'id' => $record->id,
                'attributes' => $record->attributes,
                'categories' => $record->categories,
                'description' => $record->description,
                'images' => $record->images,
                'name' => $record->name,
                'price' => $record->price,
                'regular_price' => 0,
                'sku' => $record->sku,
                'slug' => '',
                'status' => $record->status,
                'stock_quantity' => $record->stock_quantity,
                'stock_status' => $record->stock_status,
                'tax' => $record->tax,
                'type' => $record->type,
                'user_id' => $user_id
            ));
        }else{
            $data = array(array(
                'id' => $record->id,
                'name' => $record->title,
                'description' => $record->body_html,
                'parent' => NULL
            ));
        }

        $response = $http->post($linkImport, [
            $type => $data
        ]);

        return json_encode($response->json());
    }
}
