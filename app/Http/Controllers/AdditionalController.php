<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use App\Models\Charge;
use App\Models\Plan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdditionalController extends Controller
{
    public function plans(Request $request)
    {
        $plans = Plan::with('features')
                ->where('isActive', 1)
                ->where('isAdditional', 0)
                ->where('interval_unit', 'month')
                ->get();

        $token = $request->user()->password;
        return view('plans', [
            'user' => $request->user(), 
            'token' => $token,
            'plans' => $plans
        ]);
    }

    public function cancelPlan(Request $request)
    {
        $shop = $request->user();
        $token = $request->user()->password;
        $charge = Charge::where('shopify_shop_id', $shop->id)->where('status', 'ACTIVE')->first();
        if(!is_null($charge)){
            $charge->cancelled_on = Carbon::today();
            $charge->expires_on = Carbon::today();
            $charge->status = 'CANCELLED';
            $charge->save();
        }

        $shop->plan_id = null;
        $shop->shopify_freemium = false;
        $shop->save();

        return view('plans', ['user' => $request->user(), 'token' => $token]);
    }
}
