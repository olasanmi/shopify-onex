<?php

namespace App\Http\Controllers;

use App\Models\Charge;
use App\Models\Client;
use App\Models\ClientRequest;
use App\Models\EcommerceConfig;
use App\Models\Shop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class WebhookController extends Controller
{
    public function order(Request $request)
    {
        \Log::debug('Info de la request-order:', [
            'headers' => $request->header(),
            // 'payload' => $request->all(),
        ]);

        // sleep(1);

        $domain = $request->header('x-shopify-shop-domain');
        $topic = $request->header('x-shopify-topic');
        $event_id = $request->header('x-shopify-event-id');
        
        \Log::debug('Recibiendo x-shopify-shop-domain: '. $domain );
        \Log::debug('Recibiendo x-shopify-topic: '. $topic);
        \Log::debug('Recibiendo x-shopify-event-id: '. $request['id']);

        $config = EcommerceConfig::where('key', '=', 'url')->where('type_ecommerce', '=', 2)->where('value', 'LIKE', "%{$domain}%")->first();

        if(!is_null($config)){
            
            $event = DB::table('events_shopify')
                ->where('user_id', $config->user_id)
                ->where('domain', $domain)
                ->where('event_id', $request['id'])
                ->first();

            if (!is_null($event) && $topic == 'orders/create') {
                \Log::debug('Evento repetido: '. $event->event_id);
                return;
            } else {
                DB::table('events_shopify')->insert([
                    'user_id' => $config->user_id,
                    'domain' => $domain,
                    'event_id' => $request['id']
                ]);
                \Log::debug('Registrado Evento Shopify: '. $event);
            }
            
            $payload = $request->all();
            $payload['topic'] = $topic;
            $payload['onexfy_id'] = $config->user_id;

            // $link = 'https://app.onexfy.com/back/public/api/v2/shopify';
            // $link = 'https://test.onexfy.dev/back/public/api/v2/shopify';
            $link = 'http://127.0.0.1:8000/api/v2/shopify';
            
            \Log::debug('Enviando a Onexfy: '. json_encode($payload));
            $response = Http::post($link, json_encode($payload));
        }else{
            //notificar a alguien que estan llegando ordenes de una persona que no tiene configurada la integracion
        }
        return response()->json(['status' => true], 200);
    }

    public function customer(Request $request)
    {
        \Log::info('Customer-RD:', [
            'headers' => $request->header(),
            'payload' => $request->all(),
        ]);

        $clientRequest = ClientRequest::create([
            'shop' => isset($request->shop_domain) ? $request->shop_domain : '',
            'email' => isset($request->customer['email']) ? $request->customer['email'] : '',
            'data' => json_encode($request->all())
        ]);

        return response()->json(['status' => true], 200);
    }

    public function customerDeletion(Request $request)
    {
        \Log::info('Customer-DELETE:', [
            'headers' => $request->header(),
            'payload' => $request->all(),
        ]);

        $shop = $request->shop_domain;

        $config = EcommerceConfig::where('key', '=', 'url')->where('type_ecommerce', '=', 2)->where('value', 'LIKE', "%{$shop}%")->first();

        if(!is_null($config)){
            $client = Client::where('email', $request->customer['email'])->where('user_id', $config->user_id)->first();
            if(!is_null($client)){
                $client->shopify_status = false;
                $client->save();
            }
        }

        return response()->json(['status' => true], 200);
    }

    public function unistall(Request $request)
    {
        $domain = $request->domain;

        //$config = EcommerceConfig::where('key', '=', 'url')->where('type_ecommerce', '=', 2)->where('value', 'LIKE', "%{$domain}%")->first();//Quizas para notificar o algo por el estilo

        $shop = Shop::where('name', $domain)->first();
        if(!is_null($shop)){
            $charge = Charge::where('shopify_shop_id', $shop->id)->where('status', 'ACTIVE')->first();
            if(!is_null($charge)){
                $charge->cancelled_on = Carbon::today();
                $charge->expires_on = Carbon::today();
                $charge->status = 'CANCELLED';
                $charge->save();
            }

            $shop->plan_id = null;
            $shop->shopify_freemium = false;
            $shop->deleted_at = Carbon::today();
            $shop->save();
        }
        return response()->json(['status' => true], 200);
    }
}
