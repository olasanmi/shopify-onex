<?php

namespace App\Http\Controllers;

use App\Models\EcommerceConfig;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        \Log::debug('recibiendo: '. $request);
        $config = EcommerceConfig::where('user_id', $request->user_id)->where('key', 'config')->first();

        \Log::debug('buscando en la BD: '. $config);

        if (is_null($config)) {
            
            $config = EcommerceConfig::create([
                'user_id' => $request->user_id,
                'key' => 'config',
                'type_ecommerce' => 2,
                'value' => $request->mod,
            ]);

        } else {

            $config->update([
                'value' => $request->mod,
            ]);
        }

                

        return $config;

    }
}
