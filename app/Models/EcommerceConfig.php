<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EcommerceConfig extends Model
{
    //table
    protected $table = 'ecommerce_config';
    //fillable
    protected $fillable = ['key', 'value', 'type_ecommerce', 'user_id'];
}
