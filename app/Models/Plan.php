<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    /**
     * Get plan features.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function features()
    {
        return $this
            ->belongsToMany(
                Feature::class,
                'plan_feature',
                'plan_id',
                'feature_id'
            )
            ->using(PlanFeature::class)
            ->withPivot(['value', 'note'])
            ->orderBy('sort_order');
    }
}
