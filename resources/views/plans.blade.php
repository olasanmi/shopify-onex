<!DOCTYPE html>
<html>

<head>
    <title>Onexfy</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/quasar@2.0.0/dist/quasar.min.css">

    <style>
        .w3-theme {
            color: #fff !important;
            background-color: #e4e0fc !important;
        }

        .w3-theme-dark {
            color: #4d4d4d !important;
            background-color: #e4e0fc !important;
        }

        .w3-card {
            background-color: white !important;
        }

        body {
            background-color: #fff;
        }

        .input-container {
            position: relative;
            display: inline-block;
            width: 400px;
        }

        .input-container input {
            padding-right: 40px;
        }

        .input-container .eye-icon {
            position: absolute;
            top: 60%;
            right: 10px;
            transform: translateY(-50%);
            cursor: pointer;
        }

        button {
            background-color: #e4e0fc;
            border: none;
            color: #4d4d4d;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            font-weight: bold;
            padding: 10px 5px;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }

        button:hover {
            background-color: #9c96ff;
        }

        .text-primary {
            color: #7630be !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" />
</head>

<body>
    <!-- Header -->
    <header class="w3-container w3-theme w3-padding" id="myHeader">
        <div class="w3-center">
            <img src="https://onexfy.com/wp-content/uploads/2023/01/onexfy-logo-morado.png" alt="Onexfy"
                width="150px" />
        </div>
    </header>
    <div class="w3-row-padding w3-center w3-margin-top" style="padding-bottom: 14px">

        <div class="w3-half">
            <h5 style="color: #4d4d4d; text-align:left">Planes</h5>
        </div>
        <div class="w3-half" style=" text-align:right">
            <button tabindex="0" type="button"
                onclick="window.location.href='{{ URL::tokenRoute('home') }}'"
                class=""
                style="font-size: 9pt; width: 120px"><span class="q-focus-helper"></span><span
                    class="q-btn__wrapper col row q-anchor--skip"><span
                        class="q-btn__content text-center col items-center q-anchor--skip justify-center row">
                        Volver
                    </span></span></button>
        </div>
    </div>
    <div class="w3-row-padding w3-center w3-margin-top" style="padding-bottom: 14px">

        <div class="w3-quarter">
            <div class="w3-card w3-container text-center" style="min-height: 460px; text-align: left;">
                <div class="full-height q-card q-card--flat no-shadow q-my-md">
                    <div class="text-center q-card__section q-card__section--vert"><span
                            class="text-h5 text-weight-bold text-primary">
                            lite
                        </span></div>
                    <div class="q-item__section column q-mb-md q-item__section--main justify-center"
                        style="height: 7rem;">
                        <div class="row items-center justify-center"><img
                                src="https://app.onexfy.com/planes-img/Lite.svg" style="width: 23%; margin-top: -4rem;">
                        </div>
                    </div>
                    <div class="q-card__section q-card__section--vert">
                        <div class="text-center q-mt-lg">
                            <div>
                                <span class="text-h6 text-weight-bold">COP $0</span>
                                /mes ó
                            </div>
                            <div>
                                <span class="text-h6 text-weight-bold">USD $0</span>
                                /mes
                            </div>
                        </div>
                    </div>
                    <hr aria-orientation="horizontal" class="q-separator q-separator q-separator--horizontal">
                    <div class="justify-between">
                        <div class="q-my-sm q-card__section q-card__section--vert">
                            <div class="font-9 text-caption text-grey-8 q-mb-sx">

                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1">
                                        <span aria-hidden="true" role="presentation" class="text-primary q-icon" style="font-size: 12pt;">

                                            <img src="https://app.onexfy.com/planes-img/success.svg">


                                        </span>
                                    </div>
                                    <div class="col-11 font-11">
                                        1 Bancos
                                    </div>
                                </div>
                                
                                 <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Canales de pago ilimitados
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        10 productos de inventario
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Reportes
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        0 Documentos electrónicos
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Integración eCommerce (Shopify - Woocommerce)
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Contabilidad
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        POS
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Código de barras + Conteo físico
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Catálogo de WhatsApp
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Chat automatizado
                                    </div>
                                </div>
                                <div class="row q-mb-xl">
                                </div>
                            </div>
                        </div>
                        <div class="q-my-sm q-card__section q-card__section--vert">
                            <div class="q-mb-md"></div>
                        </div>
                        <div class="q-px-lg q-card__section q-card__section--vert" style="position: absolute; bottom: 0px; width: 100%;">
                            <button tabindex="0" type="button" @if(!is_null(Auth::user()->plan) && Auth::user()->plan->id == 2) disabled @endif  onclick="window.location.href='{{ route('billing', ['plan' => 2, 'shop' => Auth::user()->name, 'host' => app('request')->input('host')]) }}'" class="q-btn q-btn-item non-selectable no-outline border-primary weight-600 full-width q-btn--standard q-btn--rectangle bg-white text-primary q-btn--actionable q-focusable q-hoverable q-btn--no-uppercase q-btn--wrap"
                                style="font-size: 9pt;"><span class="q-focus-helper"></span><span
                                    class="q-btn__wrapper col row q-anchor--skip"><span
                                        class="q-btn__content text-center col items-center q-anchor--skip justify-center row">
                                        Suscribirse
                                    </span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

         <div class="w3-quarter">
            <div class="w3-card w3-container text-center" style="min-height: 460px; text-align: left;">

                <div class="full-height q-card q-card--flat no-shadow q-my-md">
                    <div class="text-center q-card__section q-card__section--vert"><span
                            class="text-h5 text-weight-bold text-primary">
                            Kepler
                        </span></div>
                    <div class="q-item__section column q-mb-md q-item__section--main justify-center"
                        style="height: 7rem;">
                        <div class="row items-center justify-center"><img
                                src="https://app.onexfy.com/planes-img/Kepler.png" style="width: 27%;"></div>
                    </div>
                    <div class="q-card__section q-card__section--vert">
                        <div class="text-center q-mt-lg">
                            <div class="text-center q-mt-lg">
                                <div>
                                    <span class="text-h6 text-weight-bold">COP $99.000</span>
                                    /mes ó
                                </div>
                                <div>
                                    <span class="text-h6 text-weight-bold">USD $25</span>
                                    /mes
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr aria-orientation="horizontal" class="q-separator q-separator q-separator--horizontal">
                    <div class="justify-between">
                        <div class="q-my-sm q-card__section q-card__section--vert">
                            <div class="font-9 text-caption text-grey-8 q-mb-sx">
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        1 Usuario
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        1 Bodega
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        1 Bancos
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Canales de pago ilimitados
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Inventario ilimitados
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Reportes
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        25 Documentos electrónicos
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Integración eCommerce (Shopify - Woocommerce)
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Contabilidad
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        POS
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Código de barras + Conteo físico
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Catálogo de WhatsApp
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Chat automatizado
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="q-my-sm q-card__section q-card__section--vert">
                            <div class="q-mb-md"></div>
                        </div>
                        <div class="q-px-lg q-card__section q-card__section--vert"
                            style="position: absolute; bottom: 0px; width: 100%;"><button tabindex="0"
                                type="button" @if(!is_null(Auth::user()->plan) && Auth::user()->plan->id == 3) disabled @endif
                                onclick="window.location.href='{{ route('billing', ['plan' => 3, 'shop' => Auth::user()->name, 'host' => app('request')->input('host')]) }}'"
                                class="q-btn q-btn-item non-selectable no-outline border-primary weight-600 full-width q-btn--standard q-btn--rectangle bg-white text-primary q-btn--actionable q-focusable q-hoverable q-btn--no-uppercase q-btn--wrap"
                                style="font-size: 9pt;"><span class="q-focus-helper"></span><span
                                    class="q-btn__wrapper col row q-anchor--skip"><span
                                        class="q-btn__content text-center col items-center q-anchor--skip justify-center row">
                                        Suscribirse
                                    </span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w3-quarter">
            <div class="w3-card w3-container text-center" style="min-height: 460px; text-align: left;">
                <div class="full-height q-card q-card--flat no-shadow q-my-md">
                    <div class="text-center q-card__section q-card__section--vert"><span
                            class="text-h5 text-weight-bold text-primary">
                            Orion
                        </span></div>
                    <div class="q-item__section column q-mb-md q-item__section--main justify-center"
                        style="height: 7rem;">
                        <div class="row items-center justify-center"><img
                                src="https://app.onexfy.com/planes-img/Orion.png"
                                style="width: 30%; margin-top: -55px;"></div>
                    </div>
                    <div class="q-card__section q-card__section--vert">
                        <div class="text-center q-mt-lg">
                            <div class="text-center q-mt-lg">
                                <div>
                                    <span class="text-h6 text-weight-bold">COP $179.000</span>
                                    /mes ó
                                </div>
                                <div>
                                    <span class="text-h6 text-weight-bold">USD $49</span>
                                    /mes
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr aria-orientation="horizontal" class="q-separator q-separator q-separator--horizontal">
                    <div class="justify-between">
                        <div class="q-my-sm q-card__section q-card__section--vert">
                            <div class="font-9 text-caption text-grey-8 q-mb-sx">
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        3 Usuario
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        3 Bodega
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        1 Bancos
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Canales de pago ilimitados
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Inventario ilimitados
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Reportes
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        50 Documentos electrónicos
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Integración eCommerce (Shopify - Woocommerce)
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Contabilidad
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        POS
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Código de barras + Conteo físico
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Catálogo de WhatsApp
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/error.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Chat automatizado
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="q-my-sm q-card__section q-card__section--vert">
                            <div class="q-mb-md"></div>
                        </div>
                        <div class="q-px-lg q-card__section q-card__section--vert"
                            style="position: absolute; bottom: 0px; width: 100%;"><button tabindex="0"
                                type="button" @if(!is_null(Auth::user()->plan) && Auth::user()->plan->id == 4) disabled @endif
                                onclick="window.location.href='{{ route('billing', ['plan' => 4, 'shop' => Auth::user()->name, 'host' => app('request')->input('host')]) }}'"
                                class="q-btn q-btn-item non-selectable no-outline border-primary weight-600 full-width q-btn--standard q-btn--rectangle bg-white text-primary q-btn--actionable q-focusable q-hoverable q-btn--no-uppercase q-btn--wrap"
                                style="font-size: 9pt;"><span class="q-focus-helper"></span><span
                                    class="q-btn__wrapper col row q-anchor--skip"><span
                                        class="q-btn__content text-center col items-center q-anchor--skip justify-center row">
                                        Suscribirse
                                    </span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w3-quarter">
            <div class="w3-card w3-container text-center" style="min-height: 460px; text-align: left;">
                <div class="full-height q-card q-card--flat no-shadow q-my-md">
                    <div class="text-center q-card__section q-card__section--vert"><span
                            class="text-h5 text-weight-bold text-primary">
                            Omega
                        </span></div>
                    <div class="q-item__section column q-mb-md q-item__section--main justify-center"
                        style="height: 7rem;">
                        <div class="row items-center justify-center"><img
                                src="https://app.onexfy.com/planes-img/Omega.png"
                                style="width: 35%; margin-top: -5rem;"></div>
                    </div>
                    <div class="q-card__section q-card__section--vert">
                        <div class="text-center q-mt-lg">
                            <div class="text-center q-mt-lg">
                                <div>
                                    <span class="text-h5 text-weight-bold">COP $349.000</span>
                                    /mes ó
                                </div>
                                <div>
                                    <span class="text-h5 text-weight-bold">USD $89</span>
                                    /mes
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr aria-orientation="horizontal" class="q-separator q-separator q-separator--horizontal">
                    <div class="justify-between">
                        <div class="q-my-sm q-card__section q-card__section--vert">
                            <div class="font-9 text-caption text-grey-8 q-mb-sx">
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        8 Usuario
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        8 Bodega
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        1 Bancos
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Canales de pago ilimitados
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Inventario ilimitados
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Reportes
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        100 Documentos electrónicos
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Integración eCommerce (Shopify - Woocommerce)
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Contabilidad
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        POS
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Código de barras + Conteo físico
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Catálogo de WhatsApp
                                    </div>
                                </div>
                                <div class="row q-mb-xs q-px-md">
                                    <div class="col-1"><span aria-hidden="true" role="presentation"
                                            class="text-primary q-icon" style="font-size: 12pt;"><img
                                                src="https://app.onexfy.com/planes-img/success.svg"></span></div>
                                    <div class="col-11 font-11">
                                        Chat automatizado
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="q-my-sm q-card__section q-card__section--vert">
                            <div class="q-mb-md"></div>
                        </div>
                        <div class="q-px-lg q-card__section q-card__section--vert"
                            style="position: absolute; bottom: 0px; width: 100%;"><button tabindex="0"
                                type="button" @if(!is_null(Auth::user()->plan) && Auth::user()->plan->id == 5) disabled @endif
                                onclick="window.location.href='{{ route('billing', ['plan' => 5, 'shop' => Auth::user()->name, 'host' => app('request')->input('host')]) }}'"
                                class="q-btn q-btn-item non-selectable no-outline border-primary weight-600 full-width q-btn--standard q-btn--rectangle bg-white text-primary q-btn--actionable q-focusable q-hoverable q-btn--no-uppercase q-btn--wrap"
                                style="font-size: 9pt;"><span class="q-focus-helper"></span><span
                                    class="q-btn__wrapper col row q-anchor--skip"><span
                                        class="q-btn__content text-center col items-center q-anchor--skip justify-center row">
                                        Suscribirse
                                    </span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>

    <footer class="w3-container w3-theme-dark w3-padding-16">
        <p>
            COPYRIGHT © 2024 Onex Media S.A.S.
            <a href="https://www.onexfy.com/" target="_blank">onexfy.com</a>
        </p>
    </footer>

    <script>
        function setLite() {
            const data = {
                token: '{{ $token }}'
            };
            // const url = `https://ecom.onexfy.com/setLite`;
            const url = `https://c27b-82-86-89-27.ngrok-free.app/setLite`;


            fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                })
                .then(response => response.json())
                .then(data => {
                    location.reload();
                })
                .catch(error => console.error(error));
        }
    </script>
</body>

</html>
