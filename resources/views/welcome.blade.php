<!DOCTYPE html>
<html>

<head>
    <title>Onexfy</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
    <style>
        .w3-theme {
            color: #fff !important;
            background-color: #e4e0fc !important;
        }

        .w3-theme-dark {
            color: #4d4d4d !important;
            background-color: #e4e0fc !important;
        }

        .w3-card {
            background-color: white !important;
        }

        body {
            background-color: #fff;
        }

        .input-container {
            position: relative;
            display: inline-block;
            width: 400px;
        }

        .input-container input {
            padding-right: 40px;
        }

        .input-container .eye-icon {
            position: absolute;
            top: 60%;
            right: 10px;
            transform: translateY(-50%);
            cursor: pointer;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" />
</head>

<body>
    <!-- Header -->
    <header class="w3-container w3-theme w3-padding" id="myHeader">
        <div class="w3-center">
            <img src="https://onexfy.com/wp-content/uploads/2023/01/onexfy-logo-morado.png" alt="Onexfy"
                width="150px" />
        </div>
    </header>

    <div class="w3-row-padding w3-center w3-margin-top" style="padding-bottom: 14px">

        <div class="w3-half">
            <div class="w3-card w3-container" style="min-height: 460px; text-align: left;">

                <p>Para realizar este proceso debes tener una cuenta en Onexfy como EMPRESA. Puedes crearla desde:
                    <a href="https://app.onexfy.com/singup" target="_blank">https://app.onexfy.com/singup</a>
                </p>

                <p><span style="font-weight: bold;">Paso 1:</span>
                    <br>
                    Dentro de la app de Onexfy en tu tienda copia el token generado
                </p>

                <p><span style="font-weight: bold;">Paso 2:</span>
                    <br>
                    En tu cuenta de Onexfy ingresa en Perfil &gt; Integraciones o ingresa directamente desde este <a
                        href="https://app.onexfy.com/dashboard/perfil?action=integraciones" target="_blank">link</a>
                    , pega en el campo Token de Shopify el token copiado y por último selecciona el impuesto con
                    el que trabajarás
                </p>

                <p><span style="font-weight: bold;">Paso 3:</span>
                    <br>Refresca el navegador para que se actualicen los cambios
                    (F5 o click a: <a href="#" onclick="reload()">Refrescar</a>)
                </p>

                <p>Para ver más detalles, visita este video para obtener el paso a paso: <a
                        href="https://youtu.be/o3gQLkeOEVU" target="_blank">Video</a></p>

                <p>De esta forma, tu cuenta Onexfy quedará sincronizada con tu tienda.</p>

            </div>
        </div>
        <div class="w3-half">
            <div class="w3-card w3-container" style="min-height: 460px">
                <h3 style="color: #4d4d4d">No configurado</h3>
                <br />
                <div class="w3-section">
                    <div class="input-container">
                        <label>Token</label>
                        <input class="w3-input" type="password" id="passwordInput" value="{{ $token }}"
                            required />
                        <span class="eye-icon">
                            <i id="eyeIcon" class="fa fa-eye" onclick="togglePasswordVisibility()"></i>
                            <i id="eyeIcon" class="fa fa-copy" onclick="copy('passwordInput')"></i>
                        </span>
                    </div>
                </div>
                <br />
                <p>Si tienes alguna duda, contacta con nosotros a info@onexfy.com.</p>
            </div>
        </div>
    </div>

    <footer class="w3-container w3-theme-dark w3-padding-16">
        <p>
            COPYRIGHT © 2024 Onex Media S.A.S.
            <a href="https://www.onexfy.com/" target="_blank">onexfy.com</a>
        </p>
    </footer>

    <script>
        function togglePasswordVisibility() {
            var passwordInput = document.getElementById("passwordInput");
            var eyeIcon = document.getElementById("eyeIcon");

            if (passwordInput.type === "password") {
                passwordInput.type = "text";
                eyeIcon.classList.remove("fa-eye");
                eyeIcon.classList.add("fa-eye-slash");
            } else {
                passwordInput.type = "password";
                eyeIcon.classList.remove("fa-eye-slash");
                eyeIcon.classList.add("fa-eye");
            }
        }

        function copy(input) {
            var copyText = document.getElementById(input);

            copyText.select();
            copyText.setSelectionRange(0, 99999);

            navigator.clipboard.writeText(copyText.value);
        }

        function reload() {
            location.reload();
        }
    </script>
</body>

</html>
