<!DOCTYPE html>
<html>

<head>
    <title>Onexfy</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
    <style>

        button {
            background-color: #e4e0fc;
            border: none;
            color: #4d4d4d;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            font-weight: bold;
            padding: 10px 5px;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }

        button:hover {
            background-color: #6532BA;
            color: white;
        }
        .custom-select {
            position: relative;
            font-family: Arial;
            
        }

        .custom-select select {
            border-radius: 5px;
        }

        .select-selected {
            background-color: #6532BA ;
            border-radius: 5px;
        }

        /* Style the arrow inside the select element: */
        .select-selected:after {
            position: absolute;
            content: "";
            top: 14px;
            right: 10px;
            width: 0;
            height: 0;
            border: 6px solid transparent;
            border-color: #fff transparent transparent transparent;
        }

        /* Point the arrow upwards when the select box is open (active): */
        .select-selected.select-arrow-active:after {
            border-color: transparent transparent #fff transparent;
            top: 7px;
        }

        /* style the items (options), including the selected item: */
        .select-items div,.select-selected {
            color: #ffffff;
            padding: 8px 16px;
            border: 1px solid transparent;
            border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
            cursor: pointer;
        }

        /* Style items (options): */
        .select-items {
            background-color: #6532BA;
            padding: 10px;
            z-index: 99;
        }

        /* Hide the items when the select box is closed: */
        .select-hide {
            display: none;
        }

        .select-items div:hover, .same-as-selected {
            background-color: rgba(0, 0, 0, 0.1);
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" />
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <!-- Header -->
    <header class="bg-slate-200 w-100 py-3" id="myHeader">
        <div class="flex items-center justify-center	">
            <img class="" src="https://onexfy.com/wp-content/uploads/2023/01/onexfy-logo-morado.png" alt="Onexfy"
                width="150px" />
        </div>
    </header>

    <div class="h-full mb-10">

        <div class="flex mt-5 px-2 gap-4" style="padding-bottom: 14px">
            
            <div class="flex-initial w-64">
                <h3 class="text-2xl ml-4" style="color: #4d4d4d; text-align:left">Configurado</h3>
            </div>
            <div class="flex-auto mr-3" style="color: #4d4d4d; text-align:right">
                <button tabindex="0" type="button" onclick="window.location.href='{{ URL::tokenRoute('plans') }}'" class="bg-slate-200 rounded px-3 py-2 text-md font-bold">
                    Ver Planes
                    
                </button>
                <button type="button" style="" onclick="window.location.href='{{ URL::tokenRoute('configs') }}'" class="bg-slate-200 rounded px-3 py-2 text-md font-bold" > 
                    Configuración
                </button>
                {{-- <button tabindex="0" type="button"
                    onclick="window.location.href='{{ URL::tokenRoute('cancelPlan') }}'"
                        class="q-btn q-btn-item non-selectable no-outline border-primary weight-600 full-width q-btn--standard q-btn--rectangle bg-white text-primary q-btn--actionable q-focusable q-hoverable q-btn--no-uppercase q-btn--wrap"
                        style="font-size: 9pt; width: 120px"><span class="q-focus-helper"></span><span
                        class="q-btn__wrapper col row q-anchor--skip"><span
                        class="q-btn__content text-center col items-center q-anchor--skip justify-center row">
                        Cancelar Plan
                            </span></span></button> --}}
                            
            </div>
        </div>
        <h5 class="text-lg w-full" style="color: #4d4d4d; margin-left:12px">¡A continuación puedes decidir cómo quieres guardar tus fácturas de Shopify en Onex!</h5>
        
        <div class=" mt-5 p-5 flex gap gap-3">  
            <div class="flex w-1/3 items-center ps-4 border border-gray-300 rounded dark:border-gray-700">
                <input id="bordered-radio-1" type="radio" value="online" name="mod" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                <label for="bordered-radio-1" class="w-full py-4 ms-2 text-sm font-medium text-gray-900 dark:text-gray-300">Como Factura Online</label>
            </div>
            <div class="flex w-1/3 items-center ps-4 border border-gray-300 rounded dark:border-gray-700">
                <input checked id="bordered-radio-2" type="radio" value="prefactura" name="mod" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                <label for="bordered-radio-2" class="w-full py-4 ms-2 text-sm font-medium text-gray-900 dark:text-gray-300">Como Prefactura</label>
            </div>
        </div>
        <div class="mt-2 pl-5">
            
            <button tabindex="0" type="button" onclick="saveConfig()" class="flex-col basis-1/4 bg-slate-200 rounded px-3 py-2 text-md font-bold">
                Guardar Configuración        
            </button>
        </div>
    </div>
        
        
        <!-- ... -->

    <footer class="bg-slate-200 mt-10 w-100 py-5">
        <p>
            COPYRIGHT © 2024 Onex Media S.A.S. / Si tienes alguna duda/problema,
            contacta con nosotros a info@onexfy.com.
            <a href="https://www.onexfy.com/" target="_blank">onexfy.com</a>
        </p>
    </footer>

    <script>
        function saveConfig() {
            mod = document.querySelector('input[name="mod"]:checked').value;
            const data = {
                token: '{{ $token }}',
                user_id: '{{ $user_id }}',
                mod: mod,
            };
            // const url = `https://ecom.onexfy.com/importData`;
            // const url = `https://ecom.onexfy.dev/importData`;
            const url = `https://c27b-82-86-89-27.ngrok-free.app/configs`;

            fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                })
                .then(response => {
                    response.json()
                    console.log(response)
                    alert(response)
                })
                .then(data => {
                    //console.log(data)
                })
                .catch(error => console.error(error));
        }
    </script>
</body>

</html>
