<!DOCTYPE html>
<html>

<head>
    <title>Onexfy</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
    <style>
        .w3-theme {
            color: #fff !important;
            background-color: #e4e0fc !important;
        }

        .w3-theme-dark {
            color: #4d4d4d !important;
            background-color: #e4e0fc !important;
        }

        .w3-card {
            background-color: white !important;
        }

        body {
            background-color: #fff;
        }

        .input-container {
            position: relative;
            display: inline-block;
            width: 400px;
        }

        .input-container input {
            padding-right: 40px;
        }

        .input-container .eye-icon {
            position: absolute;
            top: 60%;
            right: 10px;
            transform: translateY(-50%);
            cursor: pointer;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
        }

        button {
            background-color: #e4e0fc;
            border: none;
            color: #4d4d4d;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            font-weight: bold;
            padding: 10px 5px;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }

        button:hover {
            background-color: #9c96ff;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" />
</head>

<body>
    <!-- Header -->
    <header class="w3-container w3-theme w3-padding" id="myHeader">
        <div class="w3-center">
            <img src="https://onexfy.com/wp-content/uploads/2023/01/onexfy-logo-morado.png" alt="Onexfy"
                width="150px" />
        </div>
    </header>


    <div class="w3-row-padding w3-center w3-margin-top" style="padding-bottom: 14px">

        <div class="w3-half">
            <h3 style="color: #4d4d4d; text-align:left">Configurado</h3>
        </div>
        <div class="w3-half" style=" text-align:right">
            <button tabindex="0" type="button"
                onclick="window.location.href='{{ URL::tokenRoute('plans') }}'"
                class="q-btn q-btn-item non-selectable no-outline border-primary weight-600 full-width q-btn--standard q-btn--rectangle bg-white text-primary q-btn--actionable q-focusable q-hoverable q-btn--no-uppercase q-btn--wrap"
                style="font-size: 9pt; width: 120px"><span class="q-focus-helper"></span><span
                    class="q-btn__wrapper col row q-anchor--skip"><span
                        class="q-btn__content text-center col items-center q-anchor--skip justify-center row">
                        Ver Planes
                    </span></span>
            </button>
            <button tabindex="0" type="button"
                onclick="window.location.href='{{ URL::tokenRoute('configs') }}'"
                class="q-btn q-btn-item non-selectable no-outline border-primary weight-600 full-width q-btn--standard q-btn--rectangle bg-white text-primary q-btn--actionable q-focusable q-hoverable q-btn--no-uppercase q-btn--wrap"
                style="font-size: 9pt; width: 120px"><span class="q-focus-helper"></span><span
                    class="q-btn__wrapper col row q-anchor--skip"><span
                        class="q-btn__content text-center col items-center q-anchor--skip justify-center row">
                        Configuración
                    </span></span>
            </button>
            {{-- <button tabindex="0" type="button"
                    onclick="window.location.href='{{ URL::tokenRoute('cancelPlan') }}'"
                    class="q-btn q-btn-item non-selectable no-outline border-primary weight-600 full-width q-btn--standard q-btn--rectangle bg-white text-primary q-btn--actionable q-focusable q-hoverable q-btn--no-uppercase q-btn--wrap"
                    style="font-size: 9pt; width: 120px"><span class="q-focus-helper"></span><span
                        class="q-btn__wrapper col row q-anchor--skip"><span
                            class="q-btn__content text-center col items-center q-anchor--skip justify-center row">
                            Cancelar Plan
                        </span></span></button> --}}

        </div>
    </div>
    <h5 style="color: #4d4d4d; margin-left:12px">¡A continuación puedes encontrar las categorías y productos de tu tienda de Shopify,
        junto con su estado de importación a Onexfy!</h5>



    <div class="w3-row-padding w3-center w3-margin-top" style="padding-bottom: 14px">

        <div class="w3-half">
            <div class="w3-card w3-container" style="min-height: 460px; padding-bottom: 20px;">
                <h3 style="color: #4d4d4d">Categorías</h3>
                <table id="table-categories">
                    <tr>
                        <th>Nombre</th>
                        <th style="text-align: center;">Acciones</th>
                    </tr>
                </table>
                <span id="load-categories" style="text-align: center;">
                    <img src="https://media.tenor.com/lUIQnRFbpscAAAAi/loading.gif" alt="Cargando..." width="60" height="60" />
                </span>
                <span id="button-categories" style="padding-top: 20px;"></span>
            </div>
        </div>
        <div class="w3-half">
            <div class="w3-card w3-container" style="min-height: 460px; padding-bottom: 20px;">
                <h3 style="color: #4d4d4d">Productos</h3>
                <table id="table-products">
                    <tr>
                        <th>Nombre</th>
                        <th style="text-align: center;">Acciones</th>
                    </tr>
                </table>
                <span id="load-products" style="text-align: center;">
                    <img src="https://media.tenor.com/lUIQnRFbpscAAAAi/loading.gif" alt="Cargando..." width="60" height="60" />
                </span>
                <span id="button-products" style="padding-top: 20px;"></span>
            </div>
        </div>
    </div>

    <footer class="w3-container w3-theme-dark w3-padding-16">
        <p>
            COPYRIGHT © 2024 Onex Media S.A.S. / Si tienes alguna duda/problema,
            contacta con nosotros a info@onexfy.com.
            <a href="https://www.onexfy.com/" target="_blank">onexfy.com</a>
        </p>
    </footer>

    <script>
        function sync(type = 'products', idElement, record) {
            const data = {
                token: '{{ $token }}',
                user_id: '{{ $user_id }}',
                record: record,
                type: type
            };
            // const url = `https://ecom.onexfy.com/importData`;
            // const url = `https://ecom.onexfy.dev/importData`;
            const url = `https://c27b-82-86-89-27.ngrok-free.app/importData`;


            const element = document.getElementById(idElement);
            element.innerHTML = '';
            const p = document.createElement('p');
            p.textContent = 'Importada';
            element.appendChild(p);

            fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                })
                .then(response => response.json())
                .then(data => {
                    //console.log(data)
                })
                .catch(error => console.error(error));
        }

        function getData(type = 'products', last_id = 0) {
            const load = document.getElementById('load-' + type);
            load.style.display = 'block';
            const token = '{{ $token }}';
            // const url = `https://ecom.onexfy.com/getData?token=${token}&per_page=10&type=${type}&last_id=${last_id}`;
            // const url = `https://ecom.onexfy.dev/getData?token=${token}&per_page=10&type=${type}&last_id=${last_id}`;
            const url = `https://c27b-82-86-89-27.ngrok-free.app/getData?token=${token}&per_page=10&type=${type}&last_id=${last_id}`;


            fetch(url)
                .then(response => response.json())
                .then(data => {
                    const table = document.getElementById('table-' + type);

                    data.forEach(record => {
                        const row = document.createElement('tr');

                        const nameCell = document.createElement('td');
                        nameCell.style.fontWeight = 'bold';
                        const idCell = document.createElement('td');
                        idCell.style.textAlign = 'center';
                        if (record.integrate == false) {
                            const span = document.createElement('span');
                            const idElement = type + '-' + record.id
                            span.id = idElement
                            const button = document.createElement('button');
                            button.textContent = 'Enviar a Onexfy';
                            button.onclick = function() {
                                sync(type, idElement, JSON.stringify(record));
                            };
                            span.appendChild(button);
                            idCell.appendChild(span);
                        } else {
                            const p = document.createElement('p');
                            p.textContent = 'Importada';
                            idCell.appendChild(p);
                        }

                        nameCell.textContent = record.name ? record.name : record.title;

                        row.appendChild(nameCell);
                        row.appendChild(idCell);

                        table.appendChild(row);
                    });

                    const element = document.getElementById('button-' + type);
                    element.innerHTML = '';
                    const loadMoreButton = document.createElement('button');
                    element.style.display = data.length === 10 ? 'block' : 'none';
                    loadMoreButton.textContent = 'Cargar más...';
                    loadMoreButton.onclick = function() {
                        const lastId = data[data.length - 1].id;
                        getData(type, lastId);
                    };
                    element.appendChild(loadMoreButton);
                    load.style.display = 'none';
                })
                .catch(error => {
                    console.log('Error:', error);
                });
        }
        getData('categories');
        getData('products');
    </script>
</body>

</html>
